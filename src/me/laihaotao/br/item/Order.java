package me.laihaotao.br.item;

import java.util.HashSet;
import java.util.Set;

/**
 * Author:  Haotao Lai (Eric)
 * Date:    2019-12-01
 * E-mail:  haotao.lai@gmail.com
 * Website: http://laihaotao.me
 */


public class Order implements Item {

    private Set<String> tags = new HashSet<String>();

    // some other fields for an order
    private String name;
    private String createdDate;
    // ... ...

    public boolean hasTag(String t) {
        return tags.contains(t);
    }

    public String getName() {
        return name;
    }

    public static class Builder {

        Order order = new Order();

        public Builder withName(String name) {
            order.name = name;
            return this;
        }

        public Builder tag(String t) {
            order.tags.add(t);
            return this;
        }

        public void removeTag(String t) {
            order.tags.remove(t);
        }

        public Order build() {
            return order;
        }
    }

}
