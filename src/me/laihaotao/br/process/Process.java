package me.laihaotao.br.process;

import me.laihaotao.br.item.Item;
import me.laihaotao.br.rule.Rule;

/**
 * Author:  Haotao Lai (Eric)
 * Date:    2019-12-01
 * E-mail:  haotao.lai@gmail.com
 * Website: http://laihaotao.me
 */


public interface Process {

    /**
     * Process an item.
     */
    void process(Item item);

    /**
     * Attach a rule with this process explicitly.
     */
    void attachRule(Rule rule);

    /**
     * Attach a rule with this process with reflection.
     */
    void attachRule(Class<? extends Rule> clazz) throws IllegalAccessException, InstantiationException;
}
