package me.laihaotao.br.process;

import me.laihaotao.br.item.Item;
import me.laihaotao.br.item.Order;
import me.laihaotao.br.rule.Rule;

import java.util.ArrayList;
import java.util.List;

/**
 * Author:  Haotao Lai (Eric)
 * Date:    2019-12-01
 * E-mail:  haotao.lai@gmail.com
 * Website: http://laihaotao.me
 */


public class PostPaymentProcess implements Process {

    /**
     * A list to hold all rules for this process.
     */
    private List<Rule> rulePool = new ArrayList<>();

    @Override
    public void attachRule(Rule rule) {
        rulePool.add(rule);
    }

    @Override
    public void attachRule(Class<? extends Rule> clazz) throws IllegalAccessException, InstantiationException {
        Rule rule = clazz.newInstance();
        rulePool.add(rule);
    }

    @Override
    public void process(Item item) {
        if (item instanceof Order) {
            Order order = (Order) item;
            rulePool.stream()
                    .filter(rule -> rule.canApply(order))
                    .forEach(rule -> rule.apply(order, this));
        } else {
            System.err.println("PostPaymentProcess can only process order Item.");
            System.exit(1);
        }
    }
}
