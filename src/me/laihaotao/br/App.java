package me.laihaotao.br;

import me.laihaotao.br.item.Order;
import me.laihaotao.br.process.PostPaymentProcess;
import me.laihaotao.br.process.Process;
import me.laihaotao.br.rule.orderrule.*;

/**
 * Author:  Haotao Lai (Eric)
 * Date:    2019-12-01
 * E-mail:  haotao.lai@gmail.com
 * Website: http://laihaotao.me
 */


public class App {

    public static void main(String[] args) throws InstantiationException, IllegalAccessException {

        // create process and attach with rules
        Process postPaymentProcess = new PostPaymentProcess();

        postPaymentProcess.attachRule(new BookRule());
        postPaymentProcess.attachRule(new LearnSkiRule());
        postPaymentProcess.attachRule(new MembershipRule());
        postPaymentProcess.attachRule(new MUEmailRule());

        postPaymentProcess.attachRule(PhysicalProdorBookRule.class);
        postPaymentProcess.attachRule(PhysicalProdRule.class);
        postPaymentProcess.attachRule(UpgradeRule.class);

        // create orders and process them

        System.out.println("\n************** order 1 *****************");
        Order order1 = new Order.Builder()
                .withName("physical order")
                .tag("physical")
                .build();
        postPaymentProcess.process(order1);

        System.out.println("\n************** order 2 *****************");
        Order order2 = new Order.Builder()
                .withName("membership order")
                .tag("membership")
                .build();
        postPaymentProcess.process(order2);

        System.out.println("\n************** order 3 *****************");
        Order order3 = new Order.Builder()
                .withName("upgrade order")
                .tag("upgrade")
                .build();
        postPaymentProcess.process(order3);

        System.out.println("\n************** order 4 *****************");
        Order order4 = new Order.Builder()
                .withName("ski order")
                .tag("learnski")
                .build();
        postPaymentProcess.process(order4);

        System.out.println("\n************** order 5 *****************");
        Order order5 = new Order.Builder()
                .withName("physical book order")
                .tag("physical")
                .tag("book")
                .build();
        postPaymentProcess.process(order5);

    }
}
