package me.laihaotao.br.rule;

import me.laihaotao.br.item.Item;
import me.laihaotao.br.process.Process;

/**
 * Author:  Haotao Lai (Eric)
 * Date:    2019-12-01
 * E-mail:  haotao.lai@gmail.com
 * Website: http://laihaotao.me
 */


public interface Rule {

    /**
     * Check whether this me.laihaotao.br.rule can apply on a particular item or not.
     */
    boolean canApply(Item item);

    /**
     * Apply the me.laihaotao.br.rule on an item within a context.
     */
    void apply(Item item, Process context);
}
