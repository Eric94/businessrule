package me.laihaotao.br.rule.orderrule;

import me.laihaotao.br.item.Item;
import me.laihaotao.br.item.Order;
import me.laihaotao.br.process.Process;
import me.laihaotao.br.rule.Rule;

/**
 * Author:  Haotao Lai (Eric)
 * Date:    2019-12-01
 * E-mail:  haotao.lai@gmail.com
 * Website: http://laihaotao.me
 */


public abstract class OrderRule implements Rule {

    protected String targetTag;

    public OrderRule(String targetTag) {
        this.targetTag = targetTag;
    }

    @Override
    public boolean canApply(Item item) {
        if (item instanceof Order) {
            Order order = (Order) item;
            return order.hasTag(targetTag);
        }
        return false;
    }

    @Override
    public void apply(Item item, Process context) {
        if (item instanceof Order) {
            Order order = (Order) item;
            apply(order, context);
        }
    }

    abstract public void apply(Order order, Process context);
}
