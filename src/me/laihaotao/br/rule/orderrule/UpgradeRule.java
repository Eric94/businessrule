package me.laihaotao.br.rule.orderrule;

import me.laihaotao.br.item.Order;
import me.laihaotao.br.process.Process;
import me.laihaotao.br.rule.orderrule.OrderRule;

/**
 * Author:  Haotao Lai (Eric)
 * Date:    2019-12-01
 * E-mail:  haotao.lai@gmail.com
 * Website: http://laihaotao.me
 */


public class UpgradeRule extends OrderRule {

    public UpgradeRule() {
        super("upgrade");
    }

    @Override
    public void apply(Order order, Process context) {
        System.out.println(order.getName() + ": " +
                           "apply the upgrade.");
    }
}
