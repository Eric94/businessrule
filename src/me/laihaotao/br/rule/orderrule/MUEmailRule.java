package me.laihaotao.br.rule.orderrule;

import me.laihaotao.br.item.Item;
import me.laihaotao.br.item.Order;
import me.laihaotao.br.process.Process;

/**
 * Author:  Haotao Lai (Eric)
 * Date:    2019-12-01
 * E-mail:  haotao.lai@gmail.com
 * Website: http://laihaotao.me
 */


public class MUEmailRule extends OrderRule {

    public MUEmailRule() {
        super("");
    }

    @Override
    public boolean canApply(Item item) {
        if (item instanceof Order) {
            Order order = (Order) item;
            return order.hasTag("membership") || order.hasTag("upgrade");
        }
        return false;
    }

    @Override
    public void apply(Order order, Process context) {
        if (order.hasTag("membership")) {
            System.out.println(order.getName() + ": " +
                               "e-mail the owner and inform them of the activation.");
        }
        else if (order.hasTag("upgrade")) {
            System.out.println(order.getName() + ": " +
                               "e-mail the owner and inform them of the upgrade.");
        }
    }
}
