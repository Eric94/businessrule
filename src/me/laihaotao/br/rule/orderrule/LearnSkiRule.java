package me.laihaotao.br.rule.orderrule;

import me.laihaotao.br.item.Order;
import me.laihaotao.br.process.Process;

/**
 * Author:  Haotao Lai (Eric)
 * Date:    2019-12-01
 * E-mail:  haotao.lai@gmail.com
 * Website: http://laihaotao.me
 */


public class LearnSkiRule extends OrderRule {

    public LearnSkiRule() {
        super("learnski");
    }

    @Override
    public void apply(Order order, Process context) {
        System.out.println(order.getName() + ": " +
                           "add a free \"First Aid\" video to the packing slip.");
    }
}
