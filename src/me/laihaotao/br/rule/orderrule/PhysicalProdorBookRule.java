package me.laihaotao.br.rule.orderrule;

import me.laihaotao.br.item.Item;
import me.laihaotao.br.item.Order;
import me.laihaotao.br.process.Process;

/**
 * Author:  Haotao Lai (Eric)
 * Date:    2019-12-01
 * E-mail:  haotao.lai@gmail.com
 * Website: http://laihaotao.me
 */


public class PhysicalProdorBookRule extends OrderRule {

    public PhysicalProdorBookRule() {
        super("");
    }

    @Override
    public boolean canApply(Item item) {
        if (item instanceof Order) {
            Order order = (Order) item;
            return order.hasTag("physical") || order.hasTag("book");
        }
        return false;
    }

    @Override
    public void apply(Order order, Process context) {
        System.out.println(order.getName() + ": " +
                           "generate a commission payment to the agent.");

    }
}
