package me.laihaotao.br.rule.orderrule;

import me.laihaotao.br.item.Order;
import me.laihaotao.br.process.Process;

/**
 * Author:  Haotao Lai (Eric)
 * Date:    2019-12-01
 * E-mail:  haotao.lai@gmail.com
 * Website: http://laihaotao.me
 */


public class PhysicalProdRule extends OrderRule {


    public PhysicalProdRule() {
        super("physical");
    }

    @Override
    public void apply(Order order, Process context) {
        System.out.println(order.getName() + ": " +
                           "generate a packing slip for shipping.");
    }
}
