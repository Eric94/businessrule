# BusinessRule

This project is the implementation of the article [Kata16: Business Rules](http://codekata.com/kata/kata16-business-rules/).

It mainly contains three components which were defined as the top level interfaces:

- Process
- Rule
- Item

A process is the abstraction of a business process. In this case, according to the article,
the post payment process. A rule describe if a certain condition is met what kind of action
we need to perform. Rules can be attached to a process. An item is the thing a process need
to handle. In this case, will be the paid orders.

The high level description of the implementation is that **a post payment process with rules 
attached will handle various kinds of order accordingly**.

More details can go into the code.